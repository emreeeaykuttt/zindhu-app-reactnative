import 'react-native-gesture-handler'
import * as React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { ThemeProvider } from 'styled-components'

import Theme from './utils/Theme'
import UserTabNavigation from './components/user/TabNavigation'

function App() {
    return (
        <ThemeProvider theme={Theme}>
            <SafeAreaProvider>
                <UserTabNavigation />
            </SafeAreaProvider>
        </ThemeProvider>
    )
}

export default App
