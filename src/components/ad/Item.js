import React from 'react'
import { TouchableOpacity } from 'react-native'

import Box from '../helper/Box'
import Text from '../helper/Text'

function AdItem({ data, navigation }) {
    return (
        <Box py={15} mx={20} borderBottomWidth={2} borderBottomColor="yellow">
            <TouchableOpacity onPress={() => navigation.navigate('UserAdDetailView', { title: data.title })}>
                <Text fontSize={20}>{data.title}</Text>
                <Text mt={1}>
                    {data.city} - {data.district} | {data.number_employees} Kişi Çalışıyor
                </Text>
                <Box bg="navyblue" width={90} mt={3} borderRadius={5}>
                    <Text fontSize={14} textAlign="center" my={5} color="white">
                        {data.study_type}
                    </Text>
                </Box>
            </TouchableOpacity>
        </Box>
    )
}

export default AdItem
