import React from 'react'
import { FlatList, ActivityIndicator } from 'react-native'

import AdItem from './Item'

function AdList({ data, loading, onRefresh, onEndReached, loadingMore, stopFetchMore, navigation }) {
    return (
        <FlatList
            data={data}
            renderItem={({ item }) => <AdItem data={item} navigation={navigation} />}
            keyExtractor={(item) => item.id}
            onRefresh={onRefresh}
            refreshing={loading}
            onEndReached={onEndReached}
            onEndReachedThreshold={0}
            ListFooterComponent={() => loadingMore && <ActivityIndicator size="large" />}
        />
    )
}

export default AdList
