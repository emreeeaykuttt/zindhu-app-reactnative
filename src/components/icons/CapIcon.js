import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function SvgCapIcon(props) {
    return (
        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" className="" {...props}>
            <Path d="M15 15.9c-.3 0-.6 0-.8-.1l-12-3.4C.9 12 0 10.8 0 9.5 0 8.2.9 7 2.2 6.6l12-3.4c.5-.2 1.1-.2 1.6 0l12 3.4C29.1 7 30 8.2 30 9.5c0 1.3-.9 2.5-2.2 2.9l-12 3.4c-.2.1-.5.1-.8.1zm0-10.8h-.3l-12 3.4c-.4.2-.7.6-.7 1s.3.8.7 1l12 3.4c.2.1.4.1.5 0l12-3.4c.4-.1.7-.5.7-1s-.3-.8-.7-1l-12-3.4H15zM2.4 7.6z" />
            <Path d="M15 27c-4.2 0-8.4-2-10-2.8-.6-.3-1-1-1-1.7v-9.9c0-.6.4-1 1-1s1 .4 1 1v9.9c1.1.5 5.1 2.5 9 2.5 3.9 0 7.9-2 9-2.6v-9.9c0-.6.4-1 1-1s1 .4 1 1v9.9c0 .7-.4 1.4-1 1.7-1.6.9-5.8 2.9-10 2.9zM29 27c-.6 0-1-.4-1-1V9.5c0-.6.4-1 1-1s1 .4 1 1V26c0 .6-.4 1-1 1z" />
        </Svg>
    )
}

export default SvgCapIcon
