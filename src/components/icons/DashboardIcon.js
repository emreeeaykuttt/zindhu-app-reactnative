import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgDashboardIcon(props) {
    return (
        <Svg
            id="dashboard-icon_svg__Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            x={0}
            y={0}
            viewBox="0 0 20 20"
            xmlSpace="preserve"
            className=""
            {...props}
        >
            <Path
                className="dashboard-icon_svg__st0"
                d="M0 2.8C0 1.2 1.2 0 2.8 0h3.5c1.5 0 2.8 1.2 2.8 2.8v3.5c0 1.5-1.2 2.8-2.8 2.8H2.8C1.2 9.1 0 7.8 0 6.3V2.8zm2.8-1.3c-.7 0-1.3.6-1.3 1.3v3.5c0 .7.6 1.3 1.3 1.3h3.5c.7 0 1.3-.6 1.3-1.3V2.8c0-.7-.6-1.3-1.3-1.3H2.8zM10.9 2.8c0-1.5 1.2-2.8 2.8-2.8h3.5C18.8 0 20 1.2 20 2.8v3.5c0 1.5-1.2 2.8-2.8 2.8h-3.5c-1.5 0-2.8-1.2-2.8-2.8V2.8zm2.8-1.3c-.7 0-1.3.6-1.3 1.3v3.5c0 .7.6 1.3 1.3 1.3h3.5c.7 0 1.3-.6 1.3-1.3V2.8c0-.7-.6-1.3-1.3-1.3h-3.5zM10.9 13.7c0-1.5 1.2-2.8 2.8-2.8h3.5c1.5 0 2.8 1.2 2.8 2.8v3.5c0 1.5-1.2 2.8-2.8 2.8h-3.5c-1.5 0-2.8-1.2-2.8-2.8v-3.5zm2.8-1.3c-.7 0-1.3.6-1.3 1.3v3.5c0 .7.6 1.3 1.3 1.3h3.5c.7 0 1.3-.6 1.3-1.3v-3.5c0-.7-.6-1.3-1.3-1.3h-3.5zM0 13.7c0-1.5 1.2-2.8 2.8-2.8h3.5c1.5 0 2.8 1.2 2.8 2.8v3.5c0 1.5-1.2 2.8-2.8 2.8H2.8C1.2 20 0 18.8 0 17.2v-3.5zm2.8-1.3c-.7 0-1.3.6-1.3 1.3v3.5c0 .7.6 1.3 1.3 1.3h3.5c.7 0 1.3-.6 1.3-1.3v-3.5c0-.7-.6-1.3-1.3-1.3H2.8z"
            />
        </Svg>
    )
}

export default SvgDashboardIcon
