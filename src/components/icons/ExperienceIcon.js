import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function SvgExperienceIcon(props) {
    return (
        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 27" className="" {...props}>
            <Path d="M26 27H4c-2.2 0-4-1.8-4-4V8c0-2.2 1.8-4 4-4h22c2.2 0 4 1.8 4 4v15c0 2.2-1.8 4-4 4zM4 6c-1.1 0-2 .9-2 2v15c0 1.1.9 2 2 2h22c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2H4z" />
            <Path d="M19 6c-.6 0-1-.4-1-1 0-1.7-1.3-3-3-3s-3 1.3-3 3c0 .6-.4 1-1 1s-1-.4-1-1c0-2.8 2.2-5 5-5s5 2.2 5 5c0 .6-.4 1-1 1zM29 14H1c-.6 0-1-.4-1-1s.4-1 1-1h28c.6 0 1 .4 1 1s-.4 1-1 1z" />
        </Svg>
    )
}

export default SvgExperienceIcon
