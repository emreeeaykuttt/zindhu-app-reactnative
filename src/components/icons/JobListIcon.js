import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function SvgJobListIcon(props) {
    return (
        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20" stroke="currentColor" className="" {...props}>
            <Path d="M18 19.6H4c-2.1 0-3.8-1.7-3.8-3.8V8.3C.2 6.2 1.9 4.5 4 4.5h14c2.1 0 3.8 1.7 3.8 3.8v7.6c0 2-1.7 3.7-3.8 3.7zM4 6C2.8 6 1.8 7 1.8 8.3v7.6c0 1.2 1 2.2 2.2 2.2h14c1.2 0 2.2-1 2.2-2.2V8.3C20.2 7 19.2 6 18 6H4z" />
            <Path d="M16 8.9c-.4 0-.8-.3-.8-.8V6c0-2.3-1.9-4.2-4.2-4.2-2.3 0-4.2 1.9-4.2 4.2v2.1c0 .4-.3.8-.8.8s-.8-.3-.8-.8V6C5.2 2.8 7.8.2 11 .2c3.2 0 5.8 2.6 5.8 5.8v2.1c0 .5-.4.8-.8.8z" />
        </Svg>
    )
}

export default SvgJobListIcon
