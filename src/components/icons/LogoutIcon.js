import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function SvgLogoutIcon(props) {
    return (
        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" className="" {...props}>
            <Path d="M9.7 19.5c-2.5 0-4.9-1-6.7-2.8-.2-.2-.2-.5 0-.7.2-.2.5-.2.7 0 1.6 1.6 3.7 2.5 6 2.5 4.7 0 8.5-3.8 8.5-8.5s-3.8-8.5-8.5-8.5S1.2 5.3 1.2 10c0 .3-.2.5-.5.5s-.5-.2-.5-.5C.2 4.8 4.5.5 9.7.5s9.5 4.3 9.5 9.5-4.3 9.5-9.5 9.5z" />
            <Path d="M12.2 10.5H.7c-.3 0-.5-.2-.5-.5s.2-.5.5-.5h11.6c.3 0 .5.2.5.5s-.3.5-.6.5z" />
            <Path d="M10.3 13.1c-.1 0-.2 0-.3-.1-.2-.2-.3-.5-.1-.7l1.7-2.3-1.7-2.3c-.2-.2-.1-.5.1-.7.2-.2.5-.1.7.1l2.2 2.9-2.2 2.9c-.1.1-.2.2-.4.2z" />
        </Svg>
    )
}

export default SvgLogoutIcon
