import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

function SvgMailIcon(props) {
    return (
        <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 18" className="" {...props}>
            <Path d="M17.3 17.1H4.7c-2.1 0-3.8-1.7-3.8-3.8V4.7C.9 2.6 2.6.9 4.7.9h12.7c2.1 0 3.8 1.7 3.8 3.8v8.7c-.1 2-1.8 3.7-3.9 3.7zM4.7 2.4c-1.2 0-2.2 1-2.2 2.2v8.7c0 1.2 1 2.2 2.2 2.2h12.7c1.2 0 2.2-1 2.2-2.2V4.7c0-1.2-1-2.2-2.2-2.2H4.7z" />
            <Path d="M11 10.6c-.6 0-1.1-.2-1.6-.5L1.2 4.3c-.3-.3-.4-.7-.1-1.1.2-.3.7-.4 1-.2l8.2 5.8c.4.3 1 .3 1.5 0L20 3c.3-.2.8-.2 1 .2.2.3.2.8-.2 1L12.6 10c-.5.5-1 .6-1.6.6z" />
        </Svg>
    )
}

export default SvgMailIcon
