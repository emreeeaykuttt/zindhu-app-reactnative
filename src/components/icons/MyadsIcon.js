import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgMyadsIcon(props) {
    return (
        <Svg
            id="myads-icon_svg__Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            x={0}
            y={0}
            viewBox="0 0 20 22"
            xmlSpace="preserve"
            className=""
            {...props}
        >
            <Path
                className="myads-icon_svg__st0"
                d="M0 6.8c0-2.2 1.8-4 4-4h7.8c2.2 0 4 1.8 4 4v10.6c0 2.2-1.8 4-4 4H4c-2.2 0-4-1.8-4-4V6.8zm4-2.4c-1.3 0-2.4 1.1-2.4 2.4v10.6c0 1.3 1.1 2.4 2.4 2.4h7.8c1.3 0 2.4-1.1 2.4-2.4V6.8c0-1.3-1.1-2.4-2.4-2.4H4z"
            />
            <Path
                className="myads-icon_svg__st0"
                d="M7.9 1.6c-1.1 0-2 .9-2 2H4.2c0-2 1.6-3.6 3.6-3.6H16c2.2 0 4 1.8 4 4v10.6c0 2.2-1.8 4-4 4h-1c-.4 0-.8-.4-.8-.8s.4-.8.8-.8h1c1.3 0 2.4-1.1 2.4-2.4V4c0-1.3-1.1-2.4-2.4-2.4H7.9z"
            />
            <Path
                className="myads-icon_svg__st0"
                d="M4.5 7.9c0-.3.2-.5.5-.5h5.7c.3 0 .5.2.5.5s-.2.5-.5.5H5c-.2 0-.5-.2-.5-.5zM9.5 13.5c0-.3.2-.5.5-.5h.7c.3 0 .5.2.5.5s-.2.5-.5.5H10c-.3.1-.5-.2-.5-.5zM4.5 10.7c0-.3.2-.5.5-.5h5.7c.3 0 .5.2.5.5s-.2.5-.5.5H5c-.2 0-.5-.2-.5-.5z"
            />
        </Svg>
    )
}

export default SvgMyadsIcon
