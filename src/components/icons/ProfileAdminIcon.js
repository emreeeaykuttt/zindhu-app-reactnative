import * as React from 'react'
import Svg, { Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: style */

function SvgProfileAdminIcon(props) {
    return (
        <Svg
            id="profile-admin-icon_svg__Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            x={0}
            y={0}
            viewBox="0 0 19 19"
            xmlSpace="preserve"
            className=""
            {...props}
        >
            <Path
                className="profile-admin-icon_svg__st0"
                d="M2.5 15.7c-.2 0-.3-.1-.4-.2C1 13.9.5 12.1.5 10.2.5 4.9 4.8.5 10.2.5c1.5 0 2.8.3 4.1.9l.5.2-3.2 6.8c-.1.3-.4.4-.7.2-.3-.1-.4-.4-.2-.7l2.7-5.8c-1-.4-2.1-.6-3.2-.6-4.8 0-8.6 3.9-8.6 8.6 0 1.7.5 3.4 1.4 4.8.2.2.1.5-.1.7-.2.1-.3.1-.4.1z"
            />
            <Path
                className="profile-admin-icon_svg__st0"
                d="M10.2 12.6c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.2 2.5-2.5 2.5zm0-3.9c-.8 0-1.5.7-1.5 1.5s.7 1.5 1.5 1.5 1.5-.7 1.5-1.5-.7-1.5-1.5-1.5zM16.1 15.8c-.1 0-.2 0-.3-.1-.2-.2-.2-.5-.1-.7 1.2-1.3 1.8-3 1.8-4.8 0-2.8-1.6-5.4-4.2-6.6-.3-.1-.4-.4-.2-.7.1-.3.4-.4.7-.2 2.9 1.4 4.8 4.3 4.8 7.5 0 2-.7 3.9-2 5.5-.2 0-.4.1-.5.1z"
            />
            <Path
                className="profile-admin-icon_svg__st0"
                d="M15.7 15.9c-.1 0-.2 0-.3-.1l-4.2-3.6c-.2-.2-.2-.5-.1-.7.2-.2.5-.2.7-.1L16 15c.2.2.2.5.1.7-.1.1-.2.2-.4.2z"
            />
            <Path
                className="profile-admin-icon_svg__st0"
                d="M16 18.5H3c-1 0-1.8-.8-1.8-1.8S2 14.9 3 14.9h13c1 0 1.8.8 1.8 1.8s-.8 1.8-1.8 1.8zM3 15.9c-.4 0-.8.4-.8.8s.4.8.8.8h13c.4 0 .8-.4.8-.8s-.4-.8-.8-.8H3z"
            />
        </Svg>
    )
}

export default SvgProfileAdminIcon
