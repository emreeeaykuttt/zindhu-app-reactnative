import React from 'react'
import { StatusBar, Platform } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../helper/Box'

export default function Header({ children, ...props }) {
    useFocusEffect(
        React.useCallback(() => {
            if (Platform.OS === 'android') {
                StatusBar.setBackgroundColor('transparent')
                StatusBar.setTranslucent(true)
            }
        }, [])
    )

    return (
        <Box
            alignItems="center"
            pb={13}
            pt={34}
            style={{
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 2
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5
            }}
            {...props}
        >
            {children}
        </Box>
    )
}
