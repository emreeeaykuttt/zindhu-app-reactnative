import React from 'react'

import { JobListIcon, ConversationsIcon, DashboardIcon, NotificationIcon, ProfileIcon } from '../icons'
import Theme from '../../utils/Theme'
import Button from '../helper/Button'
import Box from '../helper/Box'

function UserTabBar({ state, descriptors, navigation }) {
    const focusedOptions = descriptors[state.routes[state.index].key].options

    if (focusedOptions.tabBarVisible === false) {
        return null
    }

    return (
        <Box flexDirection="row" bg="white" pb={20}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key]
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                        ? options.title
                        : route.name

                const isFocused = state.index === index

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true
                    })

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name)
                    }
                }

                return (
                    <Button key={label} pt={6} flexDirection="column" height={55} flex={1} onPress={onPress}>
                        {label === 'UserAdView' && <JobListIcon stroke={Theme.colors.navyblue} width={30} height={30} />}
                        {label === 'UserConversationView' && (
                            <ConversationsIcon stroke={Theme.colors.navyblue} width={30} height={30} />
                        )}
                        {label === 'UserDashboardView' && <DashboardIcon stroke={Theme.colors.navyblue} width={30} height={30} />}
                        {label === 'UserNotificationView' && (
                            <NotificationIcon stroke={Theme.colors.navyblue} width={30} height={30} />
                        )}
                        {label === 'UserProfileView' && <ProfileIcon stroke={Theme.colors.navyblue} width={30} height={30} />}
                        <Box size={3} bg={isFocused ? Theme.colors.navyblue : 'white'} mt={6} />
                    </Button>
                )
            })}
        </Box>
    )
}

export default UserTabBar
