import 'react-native-gesture-handler'
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'

import { NotificationIcon } from '../icons'
import Theme from '../../utils/Theme'
import UserTabBar from '../user/TabBar'
import UserAdView from '../../views/user/Ad'
import UserConversationView from '../../views/user/Conversation'
import UserDashboardView from '../../views/user/Dashboard'
import UserNotificationView from '../../views/user/Notification'
import UserProfileView from '../../views/user/Profile'
import UserAdDetailView from '../../views/user/ad/Detail'
import Button from '../helper/Button'

const UserTab = createBottomTabNavigator()
const UserDashboardStack = createStackNavigator()
const UserConversationStack = createStackNavigator()
const UserNotificationStack = createStackNavigator()
const UserProfileStack = createStackNavigator()
const UserAdStack = createStackNavigator()

function UserAdStacks() {
    return (
        <UserAdStack.Navigator>
            <UserAdStack.Screen
                name="UserAdView"
                component={UserAdView}
                options={() => {
                    return {
                        headerShown: false
                    }
                }}
            />
            <UserAdStack.Screen
                name="UserAdDetailView"
                component={UserAdDetailView}
                options={({ route, navigation }) => {
                    return {
                        title: route.params?.title || 'Boş İlan Detay',
                        headerStyle: {
                            backgroundColor: Theme.colors.yellow,
                            shadowColor: 'transparent'
                        },
                        headerTintColor: Theme.colors.navyblue,
                        headerLeft: () => (
                            <Button px={20} height="100%" onPress={() => navigation.navigate('UserAdView')}>
                                <NotificationIcon stroke={Theme.colors.navyblue} />
                            </Button>
                        )
                    }
                }}
            />
        </UserAdStack.Navigator>
    )
}

function UserConversationStacks() {
    return (
        <UserConversationStack.Navigator>
            <UserConversationStack.Screen
                name="UserConversationView"
                component={UserConversationView}
                options={() => {
                    return {
                        headerShown: false
                    }
                }}
            />
        </UserConversationStack.Navigator>
    )
}

function UserDashboardStacks() {
    return (
        <UserDashboardStack.Navigator>
            <UserDashboardStack.Screen
                name="UserDashboardView"
                component={UserDashboardView}
                options={() => {
                    return {
                        headerShown: false
                    }
                }}
            />
        </UserDashboardStack.Navigator>
    )
}

function UserNoficationStacks() {
    return (
        <UserNotificationStack.Navigator>
            <UserNotificationStack.Screen
                name="UserNotificationView"
                component={UserNotificationView}
                options={() => {
                    return {
                        headerShown: false
                    }
                }}
            />
        </UserNotificationStack.Navigator>
    )
}

function UserProfileStacks() {
    return (
        <UserProfileStack.Navigator>
            <UserProfileStack.Screen
                name="UserProfileView"
                component={UserProfileView}
                options={() => {
                    return {
                        headerShown: false
                    }
                }}
            />
        </UserProfileStack.Navigator>
    )
}

function UserTabNavigation() {
    return (
        <NavigationContainer>
            <UserTab.Navigator initialRouteName="UserAdView" tabBar={(props) => <UserTabBar {...props} />}>
                <UserTab.Screen name="UserAdView" component={UserAdStacks} />
                <UserTab.Screen name="UserConversationView" component={UserConversationStacks} />
                <UserTab.Screen name="UserDashboardView" component={UserDashboardStacks} />
                <UserTab.Screen name="UserNotificationView" component={UserNoficationStacks} />
                <UserTab.Screen name="UserProfileView" component={UserProfileStacks} />
            </UserTab.Navigator>
        </NavigationContainer>
    )
}

export default UserTabNavigation
