import queryString from 'query-string'

import { API_URL } from '@env'

export const GetAdList = async (params) => {
    let query = 'status=1&admin_confirm=approved'
    query += (params ? '&' : '') + queryString.stringify(params)

    return fetch(API_URL + '/ads?' + query, {
        method: 'GET'
    })
        .then((response) => {
            return response.json()
        })
        .catch((err) => console.log(err))
}

export const GetAdByID = async (id) => {
    let query = 'status=1&admin_confirm=approved'

    return fetch(API_URL + '/ads/' + id + '?' + query, {
        method: 'GET'
    })
        .then((response) => {
            return response.json()
        })
        .catch((err) => console.log(err))
}
