const space = []

const colors = {
    navyblue: '#050522',
    lightblue: '#5E39F3',
    pink: '#ff4084',
    yellow: '#DDF951'
}

const radii = {
    normal: 6,
    full: 9999
}

export default {
    space,
    radii,
    colors
}
