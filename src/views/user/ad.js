import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import { GetAdList } from '../../services/Ad'
import Box from '../../components/helper/Box'
import LoaderText from '../../components/helper/LoaderText'
import AdList from '../../components/ad/List'
import Header from '../../components/user/Header'
import Text from '../../components/helper/Text'

function UserAdView({ navigation }) {
    const [adLoadingRefresh, setAdLoadingRefresh] = React.useState(false)
    const [adLoadingMore, setAdLoadingMore] = React.useState(false)
    const [adLoadingMoreStatus, setAdLoadingMoreStatus] = React.useState(true)
    const [adList, setAdList] = React.useState(null)
    const [adPageCurrent, setAdPageCurrent] = React.useState(1)

    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('dark-content')
        }, [])
    )

    React.useEffect(() => {
        getAdList()
    }, [])

    const getAdList = async () => {
        setAdLoadingRefresh(true)
        const data = await GetAdList({ page: 1, per_page: 10 })
        if (data.content !== false) {
            setAdList([...data.content])
            setAdPageCurrent(2)
            setAdLoadingMoreStatus(true)
        }
        setAdLoadingRefresh(false)
    }

    const adOnEndReached = async () => {
        if (adLoadingMoreStatus) {
            setAdLoadingMore(true)
            const data = await GetAdList({ page: adPageCurrent, per_page: 10 })
            if (data.content !== false) {
                setAdList([...adList, ...data.content])
                setAdLoadingMoreStatus(true)
            } else {
                setAdLoadingMoreStatus(false)
            }
            setAdPageCurrent(adPageCurrent + 1)
            setAdLoadingMore(false)
        }
    }

    return (
        <Box as={SafeAreaView} bg="yellow">
            <Header bg="yellow">
                <Text fontSize={24} color="navyblue">
                    İlanlar
                </Text>
            </Header>

            <Box bg="white" height="100%" pb={140}>
                {adList ? (
                    <AdList
                        data={adList}
                        loading={adLoadingRefresh}
                        onRefresh={() => getAdList()}
                        onEndReached={adOnEndReached}
                        loadingMore={adLoadingMore}
                        navigation={navigation}
                    />
                ) : (
                    [1, 2, 3, 4, 5, 6, 7, 8].map((index) => (
                        <Box key={index} py={15} mx={20} borderBottomWidth={2} borderBottomColor="yellow">
                            <LoaderText width={200} />
                            <LoaderText width="100%" mt={10} />
                            <LoaderText width="100%" mt={10} />
                        </Box>
                    ))
                )}
            </Box>
        </Box>
    )
}

export default UserAdView
