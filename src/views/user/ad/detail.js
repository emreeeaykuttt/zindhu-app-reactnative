import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../../../components/helper/Box'
import Text from '../../../components/helper/Text'

function UserAdDetailView() {
    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('dark-content')
        }, [])
    )

    return (
        <Box as={SafeAreaView} bg="white" height="100%" p={15}>
            <Text fontSize={24} color="navyblue">
                Detay
            </Text>
        </Box>
    )
}

export default UserAdDetailView
