import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../../components/helper/Box'
import Header from '../../components/user/Header'
import Text from '../../components/helper/Text'

function UserConversationView() {
    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('light-content')
        }, [])
    )

    return (
        <Box as={SafeAreaView} bg="lightblue">
            <Header bg="lightblue">
                <Text fontSize={24} color="white">
                    Konuşmalar
                </Text>
            </Header>
        </Box>
    )
}

export default UserConversationView
