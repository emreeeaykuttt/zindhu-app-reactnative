import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../../components/helper/Box'
import Header from '../../components/user/Header'
import Text from '../../components/helper/Text'

function UserDashboardView() {
    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('dark-content')
        }, [])
    )

    return (
        <Box as={SafeAreaView} bg="yellow">
            <Header bg="yellow">
                <Text fontSize={24} color="navyblue">
                    Dashboard
                </Text>
            </Header>
        </Box>
    )
}

export default UserDashboardView
