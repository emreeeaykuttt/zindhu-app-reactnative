import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../../components/helper/Box'
import Header from '../../components/user/Header'
import Text from '../../components/helper/Text'

function UserNotificationView() {
    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('light-content')
        }, [])
    )

    return (
        <Box as={SafeAreaView} bg="pink">
            <Header bg="pink">
                <Text fontSize={24} color="white">
                    Bildirimler
                </Text>
            </Header>
        </Box>
    )
}

export default UserNotificationView
