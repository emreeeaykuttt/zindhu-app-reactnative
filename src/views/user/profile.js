import * as React from 'react'
import { StatusBar, SafeAreaView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import Box from '../../components/helper/Box'
import Header from '../../components/user/Header'
import Text from '../../components/helper/Text'

function UserProfileView() {
    useFocusEffect(
        React.useCallback(() => {
            StatusBar.setBarStyle('light-content')
        }, [])
    )

    return (
        <Box as={SafeAreaView} bg="navyblue">
            <Header bg="navyblue">
                <Text fontSize={24} color="white">
                    Profil
                </Text>
            </Header>
        </Box>
    )
}

export default UserProfileView
